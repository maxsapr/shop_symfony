<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{
    /**
     * @Route("/products", name="products")
     */
    public function productAction(Request $request) {
        $Products = $this->getDoctrine()->getRepository('AppBundle:Products');
        
        return $this->render('shop/products.html.twig', array(
            'products' => $Products->findAll()
        ));
    }
            
}
