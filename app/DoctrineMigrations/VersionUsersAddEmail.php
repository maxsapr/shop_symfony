<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class VersionUsersAddEmail extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $users = $schema->getTable('users');
        $users->addColumn('email', 'text');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $users = $schema->getTable('users');
        $users->dropColumn('email');
    }
}
