<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class VersionProductsAddPrice extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $products = $schema->getTable('products');
        $products->addColumn('price', 'float');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $products = $schema->getTable('products');
        $products->dropColumn('price');    
    }
}
