<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class VersionInitDb extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $usersTable = $schema->createTable('users');
        $usersTable->addColumn('user_id', 'integer');
        $usersTable->addColumn('full_name', 'text');
        $usersTable->addColumn('password', 'text');
        $usersTable->addColumn('role', 'integer');
        $usersTable->addColumn('created_at', 'integer');
        $usersTable->addColumn('updated_at', 'integer');
        $usersTable->addColumn( 'deleted_at', 'integer', [ 'default' => 0 ] );
        $usersTable->addColumn( 'activated', 'integer', [ 'default' => 1 ] );
        $usersTable->setPrimaryKey(['user_id']);
        
        $productsTable = $schema->createTable('products');
        $productsTable->addColumn('product_id', 'integer');
        $productsTable->addColumn('name', 'text');
        $productsTable->addColumn('description', 'text');
        $productsTable->setPrimaryKey(['product_id']);
        
        $ordersTable = $schema->createTable('orders');
        $ordersTable->addColumn('order_id', 'integer');
        $ordersTable->addColumn('product_id', 'integer');
        $ordersTable->addColumn('user_id', 'integer');
        $ordersTable->setPrimaryKey(['order_id']);
    }
    
    /**
     * 
     * @param Schema $schema
     */
    public function postUp(Schema $schema) {
        parent::postUp($schema);
        $this->addSql('ALTER TABLE users CHANGE user_id user_id INT AUTO_INCREMENT PRIMARY KEY;');
        $usersTable = $schema->getTable('users');
        $usersTable->addUniqueIndex(['full_name']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('users');
        $schema->dropTable('products');
        $schema->dropTable('orders');
    }
}
